package com.example.application.data.service;

import com.example.application.data.entity.SampleAddress;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class SampleAddressService {

    private final SampleAddressRepository repository;

    public SampleAddressService(SampleAddressRepository repository) {
        this.repository = repository;
    }

    public Optional<SampleAddress> get(Long id) {
        return repository.findById(id);
    }

    public SampleAddress update(SampleAddress entity) {
        return repository.save(entity);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    public Page<SampleAddress> list(Pageable pageable) {
        return repository.findAll(pageable);
    }
    
    public Page<SampleAddress> listByCustomer(String customerId,Pageable pageable) {
    	Specification<SampleAddress> spec = new Specification<SampleAddress>() {
			
			@Override
			public Predicate toPredicate(Root<SampleAddress> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				// TODO Auto-generated method stub
				return criteriaBuilder.like(root.get("customer_id"), "%"+customerId+"%");
			}
		};
        return repository.findAll(spec,pageable);
    }

    public Page<SampleAddress> list(Pageable pageable, Specification<SampleAddress> filter) {
        return repository.findAll(filter, pageable);
    }
    
    public int countByCustomerId(Long customerId) {
    	Specification<SampleAddress> spec = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("customer_id"), customerId);
    	List<SampleAddress> a = repository.findAll(spec);
    	return a.size();
    }

    public int count() {
        return (int) repository.count();
    }

}
