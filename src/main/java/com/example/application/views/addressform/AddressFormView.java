package com.example.application.views.addressform;

import java.util.List;

import com.example.application.data.entity.SampleAddress;
import com.example.application.data.entity.SamplePerson;
import com.example.application.data.service.SampleAddressService;
import com.example.application.data.service.SamplePersonService;
import com.example.application.views.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("Address Form")
@Route(value = "address-form", layout = MainLayout.class)
public class AddressFormView extends Div {

	private TextField street = new TextField("Address");
	private TextField postalCode = new TextField("Postal code");
	private TextField city = new TextField("City");
	private ComboBox<SamplePerson> customer = new ComboBox<>("Customer");
	private final SamplePersonService samplePersonService;

	private Button cancel = new Button("Cancel");
	private Button save = new Button("Add");

	private Binder<SampleAddress> binder = new Binder<>(SampleAddress.class);

	public AddressFormView(SampleAddressService addressService, SamplePersonService samplePersonService) {
		this.samplePersonService = samplePersonService;
		addClassName("address-form-view");

		add(createTitle());
		add(createFormLayout());
		add(createButtonLayout());

		binder.bindInstanceFields(this);

		clearForm();

		cancel.addClickListener(e -> clearForm());
		save.addClickListener(e -> {
			try {
				int existedData = addressService.countByCustomerId(customer.getValue().getId());
				System.out.println("existedData = "+existedData);
				if(existedData>=3) {
					Notification.show("You have reach your maximum address!");
				}else if (binder.isValid()) {
					
					binder.getBean().setCustomer_id(customer.getValue().getId());
					addressService.update(binder.getBean());
					Notification.show("Address stored.");
					clearForm();
				} else {
					Notification.show("Please fill in mandatory field.");
				}
			} catch (Exception err) {
				Notification.show("Please fill in mandatory field. ");
				err.printStackTrace();
			}
		});
	}

	private Component createTitle() {
		return new H3("Address");
	}

	private Component createFormLayout() {
		FormLayout formLayout = new FormLayout();
		formLayout.add(customer);
		formLayout.add(street, 2);
		postalCode.setAllowedCharPattern("\\d");
		postalCode.setMaxLength(5);
		postalCode.setMinLength(5);

		customer.setItemLabelGenerator(SamplePerson::getEmail);
		customer.setItems(samplePersonService.listAllCustomer());

		formLayout.add(postalCode, city);
		return formLayout;
	}

	private Component createButtonLayout() {
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.addClassName("button-layout");
		save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		buttonLayout.add(save);
		buttonLayout.add(cancel);
		return buttonLayout;
	}

	private void clearForm() {
		this.binder.setBean(new SampleAddress());
	}

}
